'use strict';

/**
 * `isAuthenticated` policy.
 */

module.exports = async (ctx, next) => {
  console.log('is authenticated',ctx.state.user);
  if (!ctx.state.user) {
    ctx.unauthorized('403 Forbidden');// Go to next policy or will reach the controller's action.
    
  }

  else {
    return await next();
  }

};
