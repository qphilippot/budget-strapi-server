'use strict';

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {
  async findCurrentUserOperations(ctx) {
    const operations = await Operations.find({
      user: ctx.state.user._id
    });

    return ctx.send(operations);
  }
};
